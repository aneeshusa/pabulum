= Pad Thai
Aneesh Agrawal
:license: CC-BY-SA-4.0
:reproducible:

Makes 3-4 servings.

== Ingredients:

- 2 cups bean sprouts
- 1/4 cup finely chopped unsalted roasted peanuts
- 6 oz rice stick noodles
- 2-3 green onions, sliced
- 2 cloves garlic, minced
- 1 package (extra) firm tofu, cubed
- (quantity?) ginger, grated
- OPTIONAL: 1.5 eggs (one whole, one just white)

=== Sauce Ingredients:

- 3-4 tablespoons soy sauce
- 3 tablespoons rice vinegar
- 1.5 tablespoons tamarind paste
- 1.5 tablespoons brown sugar
- Spritz of lime juice
- Dash of red pepper flakes
- Salt and pepper to taste


== Directions:

- Soak/cook noodles and set aside.
- While waiting for the noodles, make the sauce:
** Toss all ingredients, stirring well.
** Add sugar to taste.
- Stir fry garlic and ginger in an oiled hot wok.
- OPTIONAL: add eggs and scramble in the pan.
- Fry tofu until well browned.
- Alternate adding noodles and sauce, tossing and cooking for 5 minutes.
- Add salt, freshly ground black pepper and additional brown sugar as needed.
- Fold in bean sprouts and green onion; remove from heat. Finish cooking to taste.
- Garnish with chopped peanuts and 1-2 lime wedges.
- Enjoy, with a chili sauce if desired!
