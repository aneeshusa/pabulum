#!/usr/bin/env bash

# Copyright (C) Aneesh Agrawal and contributors
#
# This file is part of Pabulum.
#
# Pabulum is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pabulum is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Pabulum.  If not, see <http://www.gnu.org/licenses/>.

set -o errexit
set -o nounset
set -o pipefail

# GitLab pages requirement
declare -r OUTPUT_DIR="public"


main() {
    local archive_output
    archive_output="$(nix-build -A archive)"
    declare -r archive_output
    rm -rf "${OUTPUT_DIR}"
    cp \
        --recursive \
        --no-preserve=mode,xattr \
        "${archive_output}" "${OUTPUT_DIR}"
}

main "$@"
