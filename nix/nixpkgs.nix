# Copyright (C) Aneesh Agrawal and contributors
#
# This file is part of Pabulum.
#
# Pabulum is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pabulum is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Pabulum.  If not, see <http://www.gnu.org/licenses/>.

let

  # Use a pinned version of nixpkgs
  # This commit hash can be changed to update the nixpkgs pin
  version = "3badad811c600db9867f15a326efa69898299099";

  pin = import (builtins.fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/${version}.tar.gz";
    # TODO: pin sha256 when Nix 1.12 is released
  }) {};

  custom = pin.lib.fix (self: let
    callPackage = pin.lib.callPackageWith (pin // self);
    #autoArgs = pin // custom // { inherit callPackage; };
  in rec {
    inherit callPackage;

    mkName = import ./mkName.nix;
    mkMultiple = callPackage ./mkMultiple.nix {};
    renderAdoc = callPackage ./renderAdoc.nix {};
  });

in

  pin // custom
