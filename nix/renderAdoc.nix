# Copyright (C) Aneesh Agrawal and contributors
#
# This file is part of Pabulum.
#
# Pabulum is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pabulum is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Pabulum.  If not, see <http://www.gnu.org/licenses/>.

{ lib, stdenv, asciidoctor, mkName }:

# Note: currently always uses the HTML5 backend
{ src, storeDir ? null }:

let

  fullSrc = if storeDir == null then src else "${storeDir}/${src}";

in
  stdenv.mkDerivation {
    name = "${lib.removeSuffix ".adoc" (mkName src)}.html";
    src = fullSrc;
    title =
      let
        full_lines = lib.splitString "\n" (builtins.readFile fullSrc);
        lines = builtins.map (lib.removeSuffix "\n") full_lines;
      in
        lib.removePrefix "= " (builtins.elemAt lines 0);
    buildCommand = ''
      ${asciidoctor}/bin/asciidoctor \
          --backend html5 \
          --trace \
          --destination-dir "/" \
          --out-file "$out" \
          "$src"
    '';
  }
