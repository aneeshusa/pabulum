# Copyright (C) Aneesh Agrawal and contributors
#
# This file is part of Pabulum.
#
# Pabulum is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pabulum is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Pabulum.  If not, see <http://www.gnu.org/licenses/>.

{ linkFarm, mkName }:

src: derivs:

let

  namePathWrapper  = deriv: { name = deriv.name; path  = "${deriv}"; };
  nameValueWrapper = deriv: { name = deriv.name; value = deriv;      };

  entries = builtins.listToAttrs  (map nameValueWrapper derivs);

in

  linkFarm (mkName src) (map namePathWrapper  derivs) //
  entries // { inherit entries; }
