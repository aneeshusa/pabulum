# Copyright (C) Aneesh Agrawal and contributors
#
# This file is part of Pabulum.
#
# Pabulum is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pabulum is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Pabulum.  If not, see <http://www.gnu.org/licenses/>.

import os

RED = 31
GREEN = 32
BLUE = 34
MAGENTA = 35


def color(code, string):
    return '\033[' + str(code) + 'm' + string + '\033[0m'


def display_path(path):
    return color(MAGENTA, path)


def colon():
    return color(BLUE, ':')


EXCLUDE_DIRS = {
    '.git',
}


def project_path():
    # One dirname for tests dir, another for project dir
    project_dir = os.path.dirname(os.path.dirname(__file__))
    # Produces friendlier output
    return os.path.relpath(project_dir)


def paths(ignore=lambda path: False):
    project_root = project_path()
    for root, dirs, files in os.walk(project_path(), topdown=True):
        for exclude_dir in EXCLUDE_DIRS:
            if exclude_dir in dirs:
                dirs.remove(exclude_dir)
                break
        else:
            if ignore(os.path.join(root, exclude_dir)):
                dirs.remove(exclude_dir)

        for filename in files:
            full_path = os.path.join(root, filename)
            relative_path = os.path.relpath(full_path, start=project_root)
            if not ignore(relative_path):
                yield full_path


def is_code(path):
    return any(map(path.endswith, ['.nix', '.py', '.sh']))


class TestResult(object):
    pass


class Success(TestResult):
    def __init__(self, message):
        self.message = message

    def is_success(self):
        return True

    def is_failure(self):
        return False


class Failure(TestResult):
    def __init__(self, message, output):
        self.message = message
        self.output = output

    def is_success(self):
        return False

    def is_failure(self):
        return True
