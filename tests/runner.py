# Copyright (C) Aneesh Agrawal and contributors
#
# This file is part of Pabulum.
#
# Pabulum is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pabulum is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Pabulum.  If not, see <http://www.gnu.org/licenses/>.

import importlib
import os
import sys

from tests.util import color, project_path, Failure, GREEN, RED


def is_python_script(dir_entry):
    return dir_entry.name.endswith('.py') and dir_entry.is_file()


def run_tests(tests):
    any_failures = False

    for test_spec in tests:
        test_dir = os.path.join(project_path(), 'tests', *test_spec.split('.'))

        python_scripts = filter(is_python_script, os.scandir(test_dir))
        tests = sorted((entry.name for entry in python_scripts))

        for test in tests:
            test_mod_name = 'tests.{}.{}'.format(test_spec, test[:-3])
            test_mod = importlib.import_module(test_mod_name)
            if not hasattr(test_mod, 'run'):  # Not a test script
                continue

            try:
                result = test_mod.run()
            except Exception as e:
                message = 'Test \'{}\' raised an exception:'.format(test)
                result = Failure(message, str(e))

            if result.is_success():
                print('[ {} ] {}'.format(color(GREEN, 'PASS'), result.message))
            else:
                any_failures = True
                print('[ {} ] {}'.format(color(RED, 'FAIL'), result.message))
                for line in result.output.splitlines():
                    print('         {}'.format(line))

    return 1 if any_failures else 0


def main(argv):
    if sys.version_info < (3, 5):  # We use features introduced in Python 3.5
        sys.stderr.write('{}: Python 3.5 or later is needed for this script\n'
                         .format(__file__))
        return 1

    tests = ['lint']  # Only tests that are always safe and meaningful to run
    if len(argv) > 1:
        tests = argv[1:]

    return run_tests(tests)
