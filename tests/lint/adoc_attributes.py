# Copyright (C) Aneesh Agrawal and contributors
#
# This file is part of Pabulum.
#
# Pabulum is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pabulum is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Pabulum.  If not, see <http://www.gnu.org/licenses/>.

import itertools
import os
import subprocess
import textwrap

from tests.util import display_path, paths, Failure, Success


ADOC_TESTER = os.path.join(os.path.dirname(__file__), 'adoc_attributes.rb')


def is_adoc_file(path):
    return path.endswith('.adoc')


def check_attributes(path):
    command = [ADOC_TESTER, path]
    ret = subprocess.run(
        command,
        stdout=subprocess.PIPE,
        stderr=subprocess.DEVNULL,
        universal_newlines=True
    )
    output = ret.stdout.rstrip()
    return ([] if output == '' else [(path, output)])


def display_failure(failure):
    path, message = failure
    return display_path(path) + ":\n" + textwrap.indent(message, '    ')


def run():
    adoc_files = filter(is_adoc_file, paths())
    failures = list(itertools.chain(*map(check_attributes, adoc_files)))

    if len(failures) == 0:
        return Success("All .adoc files have correct attributes")
    else:
        output = '\n'.join([display_failure(failure) for failure in failures])
        return Failure("Bad attributes on these .adoc files:", output)
