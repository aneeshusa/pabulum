#!/usr/bin/env ruby

# Copyright (C) Aneesh Agrawal and contributors
#
# This file is part of Pabulum.
#
# Pabulum is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pabulum is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Pabulum.  If not, see <http://www.gnu.org/licenses/>.

require 'asciidoctor'

def check_license(filename)
  doc = Asciidoctor.load_file filename
  unless doc.attributes.include? 'license'
    raise "#{filename} is missing a license attribute"
  end
  license = doc.attributes['license']
  unless license == 'CC-BY-SA-4.0'
    raise "#{filename} has bad license #{license}"
  end
end

def main(args)
  begin
    check_license args[0]
  rescue => e
    Kernel.exit 1
  end

  Kernel.exit 0
end

main ARGV
