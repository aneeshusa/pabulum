# Copyright (C) Aneesh Agrawal and contributors
#
# This file is part of Pabulum.
#
# Pabulum is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pabulum is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Pabulum.  If not, see <http://www.gnu.org/licenses/>.

from tests.util import display_path, is_code, paths, Failure, Success
from tests.lint.shebang import is_executable, expected_shebang


CODE_LICENSE = """\
# Copyright (C) Aneesh Agrawal and contributors
#
# This file is part of Pabulum.
#
# Pabulum is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pabulum is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Pabulum.  If not, see <http://www.gnu.org/licenses/>.
"""


def has_correct_license(path):
    with open(path, 'r', encoding='utf-8') as file_to_check:
        contents = file_to_check.read()

    if is_executable(path):
        shebang_length = len(expected_shebang(path))
        contents = contents[shebang_length:]

    return contents[:len(CODE_LICENSE)] == CODE_LICENSE


def run():
    code_files = filter(is_code, paths())
    failures = [f for f in code_files if not has_correct_license(f)]

    if len(failures) == 0:
        return Success("All code files are licensed correctly")
    else:
        output = '\n'.join([display_path(path) for path in failures])
        return Failure("Bad license headers found in these files:", output)
