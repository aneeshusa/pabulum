# Copyright (C) Aneesh Agrawal and contributors
#
# This file is part of Pabulum.
#
# Pabulum is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pabulum is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Pabulum.  If not, see <http://www.gnu.org/licenses/>.

import os
import subprocess

from tests.util import Failure, Success, project_path


def run():
    paths = ['test.py', 'tests']
    paths = [os.path.join(project_path(), path) for path in paths]
    command = ['flake8', '--show-source'] + paths
    ret = subprocess.run(
        command,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True
    )

    if ret.returncode == 0:
        return Success("Tests passed flake8 lint")
    else:
        return Failure("Tests failed flake8 lint:", ret.stdout)
