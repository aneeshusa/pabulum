# Copyright (C) Aneesh Agrawal and contributors
#
# This file is part of Pabulum.
#
# Pabulum is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pabulum is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Pabulum.  If not, see <http://www.gnu.org/licenses/>.

import os
import stat

from tests.util import display_path, paths, Failure, Success


SHEBANG = """\
#!/usr/bin/env {}

"""

INTERPRETERS = {
    'py': 'python3',
    'rb': 'ruby',
    'sh': 'bash',
}


def is_executable(path):
    exec_bit = stat.S_IXUSR
    return exec_bit == (exec_bit & os.stat(path)[stat.ST_MODE])


def expected_shebang(path):
    extension = path.rpartition('.')[2]
    if extension not in INTERPRETERS:
        message = 'Unknown extension {} for file {}'.format(extension, path)
        raise Exception(message)

    return SHEBANG.format(INTERPRETERS[extension])


def has_correct_shebang(path):
    with open(path, 'r', encoding='utf-8') as file_to_check:
        contents = file_to_check.read()

    expected = expected_shebang(path)
    return contents[:len(expected)] == expected


def run():
    executables = filter(is_executable, paths())
    failures = [e for e in executables if not has_correct_shebang(e)]

    if len(failures) == 0:
        return Success("All executable shebangs are correct")
    else:
        output = '\n'.join([display_path(path) for path in failures])
        return Failure("Bad shebangs found in these files:", output)
