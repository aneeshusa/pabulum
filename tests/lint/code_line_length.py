# Copyright (C) Aneesh Agrawal and contributors
#
# This file is part of Pabulum.
#
# Pabulum is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pabulum is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Pabulum.  If not, see <http://www.gnu.org/licenses/>.

import itertools

from tests.util import (
    colon, color, display_path, is_code, paths, Failure, Success, RED
)

MAX_LINE_LENGTH = 79  # Match flake8


def display_extra_characters(line):
    # Show characters over the line limit in red
    # to make it easy to see what needs to be removed
    ok, extra = line[:MAX_LINE_LENGTH], line[MAX_LINE_LENGTH:]
    return ok + color(RED, extra)


def display_failure(failure):
    path, line_number, line = failure
    line = display_extra_characters(line)
    return display_path(path) + colon() + str(line_number) + colon() + line


def check_whitespace(path):
    trailing_whitespace = []
    with open(path, 'r', encoding='utf-8') as file_to_check:
        for line_number, line in enumerate(file_to_check):
            line = line.rstrip('\r\n')
            if len(line) > MAX_LINE_LENGTH:
                trailing_whitespace.append((path, line_number + 1, line))
    return trailing_whitespace


def run():
    code_files = filter(is_code, paths())
    failures = list(itertools.chain(*map(check_whitespace, code_files)))

    if len(failures) == 0:
        return Success("All code files fit under the line limit")
    else:
        output = '\n'.join([display_failure(failure) for failure in failures])
        return Failure("These files and lines exceed the line limit:", output)
