#!/usr/bin/env ruby

# Copyright (C) Aneesh Agrawal and contributors
#
# This file is part of Pabulum.
#
# Pabulum is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pabulum is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Pabulum.  If not, see <http://www.gnu.org/licenses/>.

require 'asciidoctor'

def check_attributes(filename)
  doc = Asciidoctor.load_file filename
  ret = []
  unless doc.attributes.include? 'reproducible'
    ret << "Missing the top-level 'reproducible' attribute"
  end
  ret
end

def main(args)
  begin
    failures = check_attributes args[0]
    if failures
      failures.each &method(:puts)
      Kernel.exit 1
    end
  rescue => e
    puts "Exception encountered during processing: #{e}"
    Kernel.exit 1
  end

  Kernel.exit 0
end

main ARGV
