# Copyright (C) Aneesh Agrawal and contributors
#
# This file is part of Pabulum.
#
# Pabulum is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pabulum is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Pabulum.  If not, see <http://www.gnu.org/licenses/>.

import os
import subprocess

from tests.util import display_path, paths, Failure, Success


ADOC_TESTER = os.path.join(os.path.dirname(__file__), 'adoc_license.rb')


def is_adoc_file(path):
    return path.endswith('.adoc')


def has_correct_license(path):
    command = [ADOC_TESTER, path]
    ret = subprocess.run(
        command,
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL,
        universal_newlines=True
    )
    return ret.returncode == 0


def run():
    # All documentation files are .adoc files, so only need to check those
    adoc_files = filter(is_adoc_file, paths())
    failures = [f for f in adoc_files if not has_correct_license(f)]

    if len(failures) == 0:
        return Success("All documenation and recipes are licensed correctly")
    else:
        output = '\n'.join([display_path(path) for path in failures])
        return Failure("Bad licensing on these files:", output)
