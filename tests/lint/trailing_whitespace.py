# Copyright (C) Aneesh Agrawal and contributors
#
# This file is part of Pabulum.
#
# Pabulum is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pabulum is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Pabulum.  If not, see <http://www.gnu.org/licenses/>.

import itertools
import re

from tests.util import colon, color, display_path, paths, Failure, Success, RED


def display_trailing_whitespace(whitespace):
    # Show trailing whitespace with printing characters and in red
    # To make it easy to see what needs to be removed
    replaced = whitespace.replace(' ', '-').replace('\t', r'\t')
    return color(RED, replaced)


def display_failure(failure):
    path, line_number, match = failure
    line = match.group(1) + display_trailing_whitespace(match.group(2))
    return display_path(path) + colon() + str(line_number) + colon() + line


def check_whitespace(path):
    CHECK_REGEX = re.compile(r'(.*?)(\s+)$')
    trailing_whitespace = []
    with open(path, 'r', encoding='utf-8') as file_to_check:
        try:
            for line_number, line in enumerate(file_to_check):
                line = line.rstrip('\r\n')
                match = CHECK_REGEX.match(line)
                if match is not None:
                    trailing_whitespace.append((path, line_number + 1, match))
        except UnicodeDecodeError:
            pass  # Not a text (UTF-8) file
    return trailing_whitespace


def ignore_path(path):
    return path in {
        "LICENSE-CC-BY-SA-4.0",  # Vendored/copyrighted - cannot change
    }


def run():
    paths_to_check = paths(ignore=ignore_path)
    failures = list(itertools.chain(*map(check_whitespace, paths_to_check)))

    if len(failures) == 0:
        return Success("No trailing whitespace found")
    else:
        output = '\n'.join([display_failure(failure) for failure in failures])
        return Failure("Trailing whitespace found on files and lines:", output)
