# Copyright (C) Aneesh Agrawal and contributors
#
# This file is part of Pabulum.
#
# Pabulum is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pabulum is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Pabulum.  If not, see <http://www.gnu.org/licenses/>.

{ pkgs ? import ./nix/nixpkgs.nix }:

let

  recipes = pkgs.callPackage ./recipes {};
  index = pkgs.renderAdoc {
    src = "index.adoc";
    storeDir = pkgs.stdenv.mkDerivation {
      name = "index.html";
      src = ./index.adoc;
      recipeList =
        let
          toListItem = recipe: recipeDrv:
            "* link:recipes/${recipe}[${recipeDrv.title}]";
          items = pkgs.lib.mapAttrsToList toListItem recipes.entries;
          sorted = builtins.sort builtins.lessThan items;
        in
          pkgs.lib.concatStringsSep "\n" sorted;
      buildCommand = ''
        mkdir -p $out
        substitute "$src" "$out/index.adoc" \
          --subst-var recipeList
      '';
    };
  };

  output = pkgs.mkMultiple "pabulum" [
    recipes
    index
  ];

  # Squashed (i.e. no symlink) version of the project
  # Used for a self-contained output
  archive = pkgs.stdenv.mkDerivation {
    name = output.name;
    src = output;
    buildCommand = ''
      cp --recursive \
         --dereference --copy-contents \
         --preserve=all --no-preserve=links \
         "$src" "$out"
    '';
  };

in

  output // { inherit archive; }
